

class TwistP33rAd1o():
    def __init__(self):
        super().__init__()
        print("init()")
        self.readWave()
        self.cypherWave()
        self.sendWave()
        
    def readWave(self):
        print("readWave()")
        
    def cypherWave(self):
        print("cypherWave()")
        
    def sendWave(self):
        print("sendWave()")
        
if __name__ == '__main__':
    tpr = TwistP33rAd1o()

